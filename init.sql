DROP DATABASE IF EXISTS my_test;
CREATE DATABASE my_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use my_test;

CREATE TABLE `ryan` (
    id int AUTO_INCREMENT,
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ip varchar(100),
    name varchar(255),

 
    PRIMARY KEY (id)
);

CREATE TABLE `dickson_ip` (
    id int AUTO_INCREMENT,
    ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ip varchar(100),
 
    PRIMARY KEY (id)
);
