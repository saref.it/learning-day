var express = require('express')
var bodyParser = require('body-parser')
var app = express()
// app.use(express.json())
var morgan = require('morgan');
var cors = require('cors')
var session = require('express-session')
const axios = require('axios');
const router = express.Router();
var multer = require('multer')
var mysql = require('mysql');
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }))
app.use(bodyParser.json({ limit: '20mb', extended: true }))

var pool = mysql.createPool({
  connectionLimit: 20,
  host: "localhost",
  user: "root",
  password: "password",
  database: "my_test",
  multipleStatements: true
});

// girlNameList
var femaleNames = ["Emily", "Hannah", "Madison", "Ashley", "Sarah", "Alexis", "Samantha", "Jessica", "Elizabeth", "Taylor", "Lauren", "Alyssa", "Kayla", "Abigail", "Brianna", "Olivia", "Emma", "Megan", "Grace", "Victoria", "Rachel", "Anna", "Sydney", "Destiny", "Morgan", "Jennifer", "Jasmine", "Haley", "Julia", "Kaitlyn"]

//random number
function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
//random girlname
function randomGirlName(min, max) {
  let index = getRndInteger(min, max);
  return femaleNames[index];
}

app.get('/', (req, res) => {
  console.log('home page display on server')
  res.send('home page')
})

app.get('/admin', (req, res) => {
  console.log("admin display on server");
  res.send("admin page");
})


app.get('/jingyee', (req, res) => {
  let min = 10, max = 60;
  let randomNumber = getRndInteger(min, max);

  console.log("random number between (" + min + ") and (" + max + ") => " + randomNumber);
  res.send("random number between (" + min + ") and (" + max + ") => " + randomNumber);
  // res.send("server ok!");
})

app.get('/dickson', (req, res) => {
  let girlName = randomGirlName(0, 29);
  console.log("random girlName =>" + girlName);
  res.send("random girlName =>" + girlName);
})

app.post('/ryan', (req, res) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  let {
    name
  } = req.body
  if (name) {
    //3. save to db
    sql = `insert into ryan (
      ip,
      name
      )values (?)`
    insertData = [
      ip,
      name
    ]
    pool.query(sql, [insertData], function (err, endresult) {
      if (err) {
        console.log(err);
        return res.status(404).send('MySql error!');
      }
    });
    /////////////    
    res.send("the name is :" + name + ", and the ip is :" + ip)

  }

  else
    res.send("go away");
});

app.post('/dickson', (req, res) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("the ip is :" + ip + ", and the filetype is :")
  res.send("the ip is :" + ip + ", and the filetype is :")
});

let port = 8085;
var server = app.listen(port, () => console.log(`Example app listening on port ${port}!`))
server.setTimeout(60000);
